import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private id: string;
  private status: boolean = false;

  constructor(private http: HttpClient,) {
    console.log('Auth Constructor');
  }

  login(username: string, password: string): Observable<JSON> {
    let credentials = this.convertToJSON(username, password);

    return this.http.post<JSON>('http://api.banha.me/auth/login', credentials);
  }

  isSigned(): boolean {
    return this.status;
  }

  private convertToJSON(username: string, password: string): JSON {
    let obj: any = {
      'username': username,
      'password': password
    };

    return <JSON> obj;
  }

  private handleError(error: HttpErrorResponse) {
    // TODO: fix error handling
    console.log(error);
    return throwError('Something went wrong!');
  }
}
