import {Component, OnInit} from '@angular/core';
import {PostService} from '../post.service';
import {Post} from '../post';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.styl']
})

export class BlogComponent implements OnInit {
  posts: Post[] = [];

  constructor(private postService: PostService) {
  }

  ngOnInit() {
    console.log('On Init Blog');
    this.postService.getPosts()
      .subscribe(list => {
        for (const item of list) {
          this.posts.push(this.postService.convertToPost(item));
        }
      });
  }
}
