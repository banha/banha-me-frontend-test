import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, map, startWith} from 'rxjs/operators';
import {PostService} from '../post.service';

export interface SimplePost {
  id: number;
  title: string;
  imageURL: string;
}

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.styl']
})

export class SearchFieldComponent implements OnInit {
  value = '';
  mode: number;
  search = new FormControl();
  posts: Observable<SimplePost[]>;

  constructor(private postService: PostService) {
    this.posts = this.search.valueChanges
      .pipe(
        debounceTime(300),
        startWith(''),
        map(state => this.filterStates(state))
      );
  }

  ngOnInit() {
  }

  openPost(id: number) {
    console.log(id);
  }

  // private filterStates(value: string): SimplePost[] {
  //   const filterValue = value.toLowerCase();
  //
  //   console.log(`Filter Value ${filterValue}`);
  //
  //   // return this.posts.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
  //   return this.posts.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
  // }

  setMode(mode: number) {
    this.mode = mode;
  }

  private filterStates(value: string): SimplePost[] {
    const filterValue = value.toLowerCase();
    const values: SimplePost[] = [];

    this.postService.searchPosts(filterValue, this.mode)
      .subscribe(list => {
        for (const item of list) {
          values.push(this.convertToSimplePost(item));
        }
      });

    return values;
    // return this.states.filter(state => state.title.toLowerCase().indexOf(filterValue) === 0);
  }

  private convertToSimplePost(obj: JSON): SimplePost {
    return {
      id: obj['id'],
      title: obj['title'],
      imageURL: obj['image_url'],
    };
  }
}
