import {Component, OnInit} from '@angular/core';
import {AppComponent} from '../app.component';


@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.styl']
})

export class MainToolbarComponent implements OnInit {
  isBlog: boolean = true;

  constructor(private navMenu: AppComponent) {
  }

  ngOnInit() {
    console.log('On Init Toolbar');
  }

  toggle() {
    this.navMenu.toggle();
  }
}
