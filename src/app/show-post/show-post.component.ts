import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {PostService} from '../post.service';
import {Post} from '../post';

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.styl']
})
export class ShowPostComponent implements OnInit {
  @Input() post: Post;

  constructor(private route: ActivatedRoute,
              private location: Location,
              private postService: PostService) {
  }

  ngOnInit() {
    this.getPost();
  }

  getPost(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.postService.getPost(id)
      .subscribe(post => this.post = this.postService.convertToPost(post));
  }

  goBack(): void {
    this.location.back();
  }

}
