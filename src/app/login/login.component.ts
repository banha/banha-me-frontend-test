import {Component, OnInit} from '@angular/core';
import {ErrorStateMatcher} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {AuthService} from '../auth.service';
import {AppComponent} from '../app.component';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})

export class LoginComponent implements OnInit {
  username: string;
  password: string;
  emailFormControl: FormControl;
  passwordFormControl: FormControl;
  matcher = new MyErrorStateMatcher();

  constructor(private location: Location,
              private userService: AuthService,
              private app: AppComponent) {

  }

  ngOnInit() {
    this.emailFormControl = new FormControl('', [Validators.required, Validators.email]);
    this.passwordFormControl = new FormControl('', [Validators.required]);
  }

  goBack() {
    this.location.back();
  }

  login() {
    let email = this.emailFormControl.valid;
    let password = this.passwordFormControl.valid;

    if (email && password) {
      try {
        this.userService.login(this.username, this.password)
          .pipe(catchError(this.handleError))
          .subscribe(status => this.checkResult(status));
      } catch (e) {
        console.log('catched');
      }
    }
    // this.goBack();
  }

  private checkResult(result: any) {
    console.log(result);

    let obj = <JSON> result;

    let id = obj.parse('id');
    let ok = obj.parse('ok');
    let status = obj.parse('status');

    console.log(id);
    console.log(ok);
    console.log(status);

    if (result['id']) {
      console.log('ok');
    } else {
      console.log('not ok');
      this.loginFailed();
    }
  }

  private handleError(error: HttpErrorResponse) {
    // TODO: fix error handling
    console.log(error);
    // this.loginFailed();
    return throwError('Something went wrong!');
  }

  private loginFailed() {
    this.emailFormControl.setValue('dfnd', {emitModelToViewChange: true});
  }
}
