import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SelectSearchTypeComponent} from './select-search-type.component';

describe('SelectSearchTypeComponent', () => {
  let component: SelectSearchTypeComponent;
  let fixture: ComponentFixture<SelectSearchTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectSearchTypeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSearchTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
