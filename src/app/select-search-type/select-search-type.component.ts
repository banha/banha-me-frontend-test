import {Component, OnInit} from '@angular/core';
import {SearchFieldComponent} from '../search-field/search-field.component';

export interface Mode {
  id: number;
  name: string;
}

@Component({
  selector: 'app-select-search-type',
  templateUrl: './select-search-type.component.html',
  styleUrls: ['./select-search-type.component.styl']
})

export class SelectSearchTypeComponent implements OnInit {
  modes: Mode[] = [
    {id: 1, name: 'Search by title'},
    {id: 2, name: 'Search by text'},
    {id: 3, name: 'Search by title and text'},
  ];

  constructor(private searchComponent: SearchFieldComponent) {
  }

  ngOnInit() {
  }

  setType(id: number) {
    this.searchComponent.setMode(id);
  }

}
