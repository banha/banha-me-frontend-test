import {Injectable} from '@angular/core';
import {Post} from './post';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ConfigService} from './config.service';
import {Config} from './config';
import {catchError, distinctUntilChanged, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PostService {
  private urls: Config;

  constructor(private http: HttpClient,
              private config: ConfigService) {
    this.config.getConfig()
      .subscribe(
        (data: Config) => this.urls = data,
        error => this.handleError(error)
      );
  }

  getPosts(): Observable<JSON[]> {
    return this.http.get<JSON[]>('http://api.banha.me/posts')
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  getPost(id): Observable<JSON> {
    return this.http.get<JSON>(`http://api.banha.me/posts/${id}`)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  searchPosts(sample: string, type: number): Observable<JSON[]> {
    return this.http.get<JSON[]>(`http://api.banha.me/posts/search/${sample}/${type}`)
      .pipe(
        distinctUntilChanged(),
        catchError(this.handleError)
      );
  }


  sendPost(post: Post) {
    return this.http.post<JSON>('http://api.banha.me/posts', this.convertToJSON(post, 0))
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  updatePost(post: Post, id: number) {
    return this.http.put<JSON>(`http://api.banha.me/posts/${id}`, this.convertToJSON(post, id))
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  deletePost(id: number) {
    return this.http.delete(`http://api.banha.me/posts/${id}`)
      .pipe(
        catchError(this.handleError)
      ).subscribe();
  }

  // TODO: implement via RxJS
  convertToPost(obj: JSON): Post {
    let post = new Post();

    post.id = obj['id'];
    post.title = obj['title'];
    post.imageURL = obj['image_url'];
    post.text = obj['text'];
    post.authorId = obj['author_id'];
    post.authorFullName = obj['author_full_name'];
    post.authorFirstName = obj['author_first_name'];
    post.authorLastName = obj['author_last_name'];
    post.authorPhotoURL = obj['author_photo_url'];
    post.created_at = obj['created_at'];
    post.updated_at = obj['updated_at'];

    return post;
  }

  private convertToJSON(post: Post, id: number): JSON {
    let obj: any = {
      'id': id,
      'title': 'What is Lorem Ipsum?',
      'author_id': '1',
      'image_url': 'https://quinncreative.com/wp-content/uploads/lorem_ipsum_by_neosh.jpg',
      'text': 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
      'created_at': '1019-02-25 10:08:00 +2:00',
      'updated_at': '1019-02-25 10:08:00 +2:00'
    };

    return <JSON> obj;
  }

  private handleError(error: HttpErrorResponse) {
    // TODO: fix error handling
    console.log(error);
    return throwError('Something went wrong!');
  }
}
