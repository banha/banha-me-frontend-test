import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ConfigService {
  private apiConfigUrl = 'assets/config/api.json';

  constructor(private http: HttpClient) {
  }

  getConfig() {
    return this.http.get(this.apiConfigUrl)
      .pipe(
        retry(5),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    // TODO: fix error handling
    console.log(error);
    return throwError('Something went wrong!');
  }
}
