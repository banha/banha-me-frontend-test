import {Component, OnInit} from '@angular/core';
import {Post} from '../post';
import {PostService} from '../post.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.styl']
})

export class DashboardComponent implements OnInit {
  posts: Post[] = [];

  constructor(private postService: PostService) {
  }

  ngOnInit(): void {
    this.getItems();
  }

  getItems() {
    this.postService.getPosts()
      .subscribe(list => {
        for (const item of list) {
          this.posts.push(this.postService.convertToPost(item));
        }
      });
  }

  deleteItem(id) {
    this.postService.deletePost(id);
    this.posts = [];
    this.getItems();
  }
}
