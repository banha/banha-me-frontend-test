export class Post {
  id: number;
  title: string;
  imageURL: string;
  text: string;
  authorId: number;
  authorFullName: string;
  authorFirstName: string;
  authorLastName: string;
  authorPhotoURL: string;
  created_at: string;
  updated_at: string;
}
